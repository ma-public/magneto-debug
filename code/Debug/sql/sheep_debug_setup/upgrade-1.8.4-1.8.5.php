<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();
$connection->modifyColumn(
    $this->getTable('sheep_debug/request_info'),
    'info',
    [
        'type' => Varien_Db_Ddl_Table::TYPE_BLOB,
        'length' => 33554432,
        'comment' => 'Serialized Info',
        'nullable' => false,
        'default'  => ''
    ]
);
$connection->addColumn(
    $installer->getTable('sheep_debug/request_info'),
    'user_agent',
    [
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 255,
        'comment' => 'HTTP User Agent.',
        'nullable' => true,
        'after' => 'ip'
    ]
);
$connection->addColumn(
    $installer->getTable('sheep_debug/request_info'),
    'reason_code',
    [
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'length' => 100,
        'comment' => 'Persist Reason.',
        'nullable' => true,
        'after' => 'info'
    ]
);
$installer->endSetup();
