<?php

/**
 * Filter
 *
 * @author Fabrizio Branca
 * @since 2014-08-16
 */
class Sheep_Debug_Block_Filter extends Mage_Core_Block_Template
{

    protected $_template = 'sheep_debug/aoe_profiler/filter.phtml';

}
