<?php

/**
 * Class Sheep_Debug_Block_Logging
 *
 * @category Sheep
 * @package  Sheep_Debug
 * @license  Copyright: Pirate Sheep, 2016
 * @link     https://piratesheep.com
 */
class Sheep_Debug_Block_Logging extends Sheep_Debug_Block_Panel
{
    /**
     * @return array
     */
    public function getLogging()
    {
        return $this->getRequestInfo()->getRequestLogging();
    }
}
