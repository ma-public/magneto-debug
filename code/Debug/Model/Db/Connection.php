<?php

/**
 * Class Sheep_Debug_Model_Db_Connection
 */
class Sheep_Debug_Model_Db_Connection
{
    protected $name;
    protected $queries;
    protected $totalNumQueries;
    protected $totalElapsedSecs;


    /**
     * Sheep_Debug_Model_Db_Connection constructor.
     * @param string $connectionName
     */
    public function __construct($connectionName)
    {
        $this->name = $connectionName;
    }


    /**
     * @return Magento_Db_Adapter_Pdo_Mysql
     */
    protected function getConnection()
    {
        return Mage::getSingleton('core/resource')->getConnection($this->name);
    }


    /**
     * Init
     */
    public function init()
    {
        if ($dbConnection = $this->getConnection()) {
            /* @var Zend_Db_Profiler $profiler */
            $profiler = $dbConnection->getProfiler();

            $this->totalNumQueries = $profiler->getTotalNumQueries();
            $this->totalElapsedSecs = $profiler->getTotalElapsedSecs();
            if ($profiler instanceof Sheep_Debug_Model_Db_Profiler) {
                $this->queries = $profiler->getQueryModels() ?: [];
            }
        }
    }


    /**
     * Replace Profiler
     */
    public function replaceProfiler()
    {
        if ($dbConnection = $this->getConnection()) {
            /* @var Sheep_Debug_Model_Db_Profiler $stackTraceProfiler
            use customer Zend Db Profiler that also records stack traces */
            $stackTraceProfiler = Mage::getModel('sheep_debug/db_profiler');
            $stackTraceProfiler->setCaptureStacktraces(Mage::helper('sheep_debug')->canEnableSqlStacktrace());
            $stackTraceProfiler->replaceProfiler($dbConnection);
        }
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @return Sheep_Debug_Model_Query[]
     */
    public function getQueries()
    {
        return $this->queries;
    }


    /**
     * @param $index
     * @return Sheep_Debug_Model_Query
     */
    public function getQuery($index)
    {
        return empty($this->queries[$index]) ? null : $this->queries[$index];
    }


    /**
     * @return mixed
     */
    public function getTotalNumQueries()
    {
        return $this->totalNumQueries;
    }


    /**
     * @return mixed
     */
    public function getTotalElapsedSecs()
    {
        return $this->totalElapsedSecs;
    }
}
