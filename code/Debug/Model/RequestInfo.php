<?php

/**
 * Class Sheep_Debug_Model_RequestInfo
 *
 * @category Sheep
 * @package  Sheep_Debug
 * @license  Copyright: Pirate Sheep, 2016
 * @link     https://piratesheep.com
 *
 * @method boolean getIsStarted()
 * @method void setIsStarted(boolean $value)
 * @method Sheep_Debug_Model_RequestInfo setToken(string $value)
 * @method string getHttpMethod()
 * @method Sheep_Debug_Model_RequestInfo setHttpMethod(string $value)
 * @method int getStoreId()
 * @method Sheep_Debug_Model_RequestInfo setStoreId(int $value)
 * @method string getRequestPath()
 * @method Sheep_Debug_Model_RequestInfo setRequestPath(string $value)
 * @method int getResponseCode()
 * @method Sheep_Debug_Model_RequestInfo setResponseCode(int $value)
 * @method string getIp()
 * @method Sheep_Debug_Model_RequestInfo setIp(string $value)
 * @method string getSessionId()
 * @method Sheep_Debug_Model_RequestInfo setSessionId(string $value)
 * @method string getDate()
 * @method Sheep_Debug_Model_RequestInfo setDate(string $value)
 * @method Sheep_Debug_Model_RequestInfo setRenderingTime(float $value)
 * @method float getQueryTime()
 * @method Sheep_Debug_Model_RequestInfo setQueryTime(float $value)
 * @method int getQueryCount()
 * @method Sheep_Debug_Model_RequestInfo setQueryCount(int $value)
 * @method Sheep_Debug_Model_RequestInfo setTime(float $value)
 * @method Sheep_Debug_Model_RequestInfo setPeakMemory(int $value)
 * @method Sheep_Debug_Model_RequestInfo setReasonCode(int $value)
 * @method Sheep_Debug_Model_RequestInfo setUserAgent(string $value)
 * @method  getInfo()
 * @method Sheep_Debug_Model_RequestInfo setInfo($value)
 */
class Sheep_Debug_Model_RequestInfo extends Mage_Core_Model_Abstract
{
    /** @var Sheep_Debug_Model_Logging */
    protected $logging;

    /** @var array */
    protected $requestLogging = [];

    /** @var Sheep_Debug_Model_Controller */
    protected $action;

    /** @var Sheep_Debug_Model_Design */
    protected $design;

    /** @var Sheep_Debug_Model_Block[] */
    protected $blocks = array();

    /** @var Sheep_Debug_Model_Model[] */
    protected $models = array();

    /** @var Sheep_Debug_Model_Collection[] */
    protected $collections = array();

    /** @var Sheep_Debug_Model_Db_Connection[]*/
    protected $dbConnections = array();

    protected $caches = [];
    protected $cachesTime;

    protected $timers = array();
    protected $stackLog;
    protected $events;
    protected $observers;
    protected $eventsTime;

    /** @var Sheep_Debug_Model_Email[] */
    protected $emails = array();

    /**
     * @var array
     */
    protected $exceptions = array();


    /**
     * Initialises logging by registering commong log files
     */
    public function initLogging()
    {
        $helper = Mage::helper('sheep_debug');
        $this->logging = Mage::getModel('sheep_debug/logging');

        $this->logging->addFile($helper->getLogFilename($this->getStoreId()));
        $this->logging->addFile($helper->getExceptionLogFilename($this->getStoreId()));
        $logs = glob(Mage::getBaseDir('log') . '/*.log');
        foreach ($logs as $path) {
            $this->logging->addFile(basename($path));
        }

        Mage::dispatchEvent('sheep_debug_init_logging', array('logging' => $this->logging));

        $this->logging->startRequest();
    }


    /**
     * Marks logging as completed.
     */
    public function completeLogging()
    {
        $logs = glob(Mage::getBaseDir('log') . '/*.log');
        foreach ($logs as $path) {
            $this->logging->addFile(basename($path));
        }
        $this->getLogging()->endRequest();
    }


    /**
     * Returns timers registered by Varien Profiler
     *
     * @see Varien_Profiler
     * @return array
     */
    public function getTimers()
    {
        return $this->timers;
    }

    /**
     * Return stackLog registered by Varien Profiler
     *
     * @see Varien_Profiler
     * @return mixed
     */
    public function getStackLog()
    {
        return $this->stackLog;
    }

    /**
     * Prepare timers data to table output
     *
     * @return array
     */
    public function getProfilerData()
    {
        $profilerData = array();

        foreach ($this->getTimers() as $name => $timer) {
            $profilerData[] = array(
                'name'      => $name,
                'time (ms)' => round($timer['sum'] * 1000, 2), // ms
                'count'     => $timer['count'],
                'mem (mb)'  => $timer['realmem'] / pow(1024, 2), // mb
            );
        }

        return $profilerData;
    }

    /**
     * Returns list of events dispatched during request.
     * This is extracted from data captured by Varien Profiler
     *
     * @return array
     */
    public function getEvents()
    {
        if ($this->events === null) {
            $this->events = array();

            foreach ($this->getTimers() as $timerName => $timer) {
                if (strpos($timerName, 'DISPATCH EVENT:') === 0) {
                    $this->events[str_replace('DISPATCH EVENT:', '', $timerName)] = array(
                        'name'     => str_replace('DISPATCH EVENT:', '', $timerName),
                        'count'    => $timer['count'],
                        'time'     => round($timer['sum'] * 1000, 2) . ' ms', // ms
                        'mem_diff' => $timer['realmem'] / pow(1024, 2), // mb
                    );
                }
            }
        }

        return $this->events;
    }


    /**
     * Returns a list of called observers during request.
     * Observers are determined based on Varien Profiler timers
     *
     * @return array
     */
    public function getObservers()
    {
        if ($this->observers === null) {
            $this->observers = array();

            foreach ($this->getTimers() as $timerName => $timer) {
                if (strpos($timerName, 'OBSERVER') === 0) {
                    $this->observers[] = array(
                        'name'     => $timerName,
                        'count'    => $timer['count'],
                        'time'     => round($timer['sum'] * 1000, 2) . ' ms', // ms
                        'mem_diff' => $timer['realmem'] / pow(1024, 2), // MB
                    );
                }
            }
        }

        return $this->observers;
    }

    /**
     * Returns total time used for events and observers during request.
     *
     * @return float
     */
    public function getEventsTime()
    {
        if ($this->eventsTime === null) {
            $this->eventsTime = 0;

            foreach ($this->getTimers() as $timerName => $timer) {
                if (strpos($timerName, 'DISPATCH EVENT:') === 0) {
                    $this->eventsTime += $timer['sum'];
                }
            }
        }

        return $this->eventsTime;
    }

    public function getCaches()
    {
        return $this->caches;
    }

    public function getCacheMetrics()
    {
        $metrics = array(
            'load' => 0,
            'save' => 0,
            'clean' => 0,
            'hit'  => 0,
            'miss' => 0,
            'time' => 0
        );
        foreach ($this->getCaches() as $log) {
            $metrics['time']+= $log['time'];
            $metrics[$log['action']]++;
            if ($log['action'] === 'load') {
                $metrics[$log['hit'] ? 'hit' : 'miss']++;
            }
        }

        $this->cachesTime = $metrics['time'];
        $metrics['time'] = round($metrics['time'] * 1000) . ' ms';

        return $metrics;
    }

    public function getCacheTime()
    {
        if ($this->cachesTime === null) {
            $this->cachesTime = 0;

            foreach ($this->getCaches() as $cache) {
                $this->cachesTime += $cache['time'];
            }
        }

        return $this->cachesTime;
    }


    /**
     * @param $caches
     */
    public function setCaches($caches)
    {
        $this->caches = $caches;
    }

    /**
     * Sets Varien Profiler timers
     *
     * @param array $timers
     */
    public function setTimers($timers)
    {
        $this->timers = $timers;
    }

    /**
     * Sets Varien Profiler stack log
     *
     * @param array $stackLog
     */
    public function setStackLog(array $stackLog)
    {
        $this->stackLog = $stackLog;
    }

    /**
     * Sets request logging
     *
     * @param array $requestLogging
     */
    public function setRequestLogging(array $requestLogging)
    {
        $this->requestLogging = $requestLogging;
    }

    /**
     * Adds sent e-mail info
     *
     * @param Sheep_Debug_Model_Email $email
     */
    public function addEmail(Sheep_Debug_Model_Email $email)
    {
        $this->emails[] = $email;
    }


    /**
     * @return mixed
     */
    public function getReasonCode()
    {
        return (int) $this->getData('reason_code');
    }


    /**
     * Returns e-mails sent during request
     *
     * @return Sheep_Debug_Model_Email[]
     */
    public function getEmails()
    {
        return $this->emails;
    }


    /**
     * Returns captured logging information
     *
     * @return Sheep_Debug_Model_Logging
     */
    public function getLogging()
    {
        return $this->logging;
    }

    /**
     * Returns request logging data
     *
     * @return array
     */
    public function getRequestLogging()
    {
        return $this->requestLogging;
    }

    /**
     * Captures information from controller
     *
     * @param Mage_Core_Controller_Varien_Action $controllerAction
     */
    public function initController($controllerAction = null)
    {
        /** @var Sheep_Debug_Model_Controller $controller */
        $controller = Mage::getModel('sheep_debug/controller');
        $controller->init($controllerAction);
        $this->action = $controller;
    }


    /**
     * Returns request/response/controller model
     *
     * @return Sheep_Debug_Model_Controller
     */
    public function getController()
    {
        return $this->action;
    }


    /**
     * Captures layout information
     *
     * @param Mage_Core_Model_Layout $layout
     * @param Mage_Core_Model_Design_Package $design
     */
    public function addLayout(Mage_Core_Model_Layout $layout, Mage_Core_Model_Design_Package $design)
    {
        $this->getDesign()->init($layout, $design);
    }


    /**
     * Returns design model
     *
     * @return Sheep_Debug_Model_Design
     */
    public function getDesign()
    {
        return $this->design;
    }


    /**
     * Adds block
     *
     * @param Mage_Core_Block_Abstract $block
     * @return Sheep_Debug_Model_Block
     */
    public function addBlock(Mage_Core_Block_Abstract $block)
    {
        $blockInfo = Mage::getModel('sheep_debug/block');
        $blockInfo->init($block);
        $key = $blockInfo->getName();

        return $this->blocks[$key] = $blockInfo;
    }


    /**
     * Returns block information associated to specified block name
     *
     * @param $blockName
     * @return Sheep_Debug_Model_Block
     * @throws Exception
     */
    public function getBlock($blockName)
    {
        if (!array_key_exists($blockName, $this->blocks)) {
            throw new Exception('Unable to find block with name ' . $blockName);
        }

        return $this->blocks[$blockName];
    }


    /**
     * Returns information about instantiated/rendered blocks
     *
     * @return Sheep_Debug_Model_Block[]
     */
    public function getBlocks()
    {
        return $this->blocks;
    }


    /**
     * Returns information about instantiated/rendered blocks
     *
     * @return array
     */
    public function getBlocksAsArray()
    {
        $helper = Mage::helper('sheep_debug');
        $data = array();
        foreach ($this->getBlocks() as $block) {
            $data[] = array(
                'name'     => $block->getName(),
                'class'    => $block->getClass(),
                'template' => $block->getTemplateFile(),
                'time (ms)' => $block->getRenderedDuration() ? round($block->getRenderedDuration()) : '-',
                'count'    => $block->getRenderedCount()
            );
        }

        return $data;
    }


    /**
     * Adds loaded collection
     *
     * @param Varien_Data_Collection_Db $collection
     */
    public function addCollection(Varien_Data_Collection_Db $collection)
    {
        $info = Mage::getModel('sheep_debug/collection');
        $info->init($collection);
        $key = $info->getClass();

        if (!array_key_exists($key, $this->collections)) {
            $this->collections[$key] = $info;
        }

        $this->collections[$key]->incrementCount();
    }


    /**
     * Returns information about loaded collections
     *
     * @return Sheep_Debug_Model_Collection[]
     */
    public function getCollections()
    {
        return $this->collections;
    }


    /**
     * Returns information about loaded collections
     *
     * @return array
     */
    public function getCollectionsAsArray()
    {
        $data = array();

        foreach ($this->getCollections() as $collection) {
            $data[] = array(
                'type'  => $collection->getType(),
                'class' => $collection->getClass(),
                'sql'   => $collection->getQuery(),
                'count' => $collection->getCount()
            );
        }

        return $data;
    }


    /**
     * Adds loaded model
     *
     * @param Mage_Core_Model_Abstract $model
     */
    public function addModel(Mage_Core_Model_Abstract $model)
    {
        $modelInfo = Mage::getModel('sheep_debug/model');
        $modelInfo->init($model);
        $key = $modelInfo->getClass();

        if (!array_key_exists($key, $this->models)) {
            $this->models[$key] = $modelInfo;
        }

        $this->models[$key]->incrementCount();
    }


    /**
     * Returns information about captured models
     *
     * @return Sheep_Debug_Model_Model[]
     */
    public function getModels()
    {
        return $this->models;
    }


    /**
     * Returns information about captured models
     *
     * @return array
     */
    public function getModelsAsArray()
    {
        $data = array();

        foreach ($this->getModels() as $model) {
            $data[] = array(
                'resource_name' => $model->getResource(),
                'class'         => $model->getClass(),
                'count'         => $model->getCount()
            );
        }

        return $data;
    }


    /**
     * @param string $connectionName
     */
    public function addDbConnection($connectionName)
    {
        /** @var  Sheep_Debug_Model_Db_Connection $model */
        $model = Mage::getModel('sheep_debug/db_connection', $connectionName);
        $model->replaceProfiler();
        array_push($this->dbConnections, $model);
    }


    /**
     * Init Db Connections
     */
    public function initDbConnections()
    {
        $queryCount = 0;
        $queryTime = 0;

        foreach ($this->dbConnections as $dbConnection) {
            $dbConnection->init();
            $queryCount += $dbConnection->getTotalNumQueries();
            $queryTime += $dbConnection->getTotalElapsedSecs();
        }

        $this->setQueryCount($queryCount);
        $this->setQueryTime($queryTime);
    }


    /**
     * Returns db connections stats including sql queries.
     *
     * @return Sheep_Debug_Model_Db_Connection[]
     */
    public function getDbConnections()
    {
        return $this->dbConnections;
    }


    /**
     * @param $index
     * @return Sheep_Debug_Model_Db_Connection | null
     */
    public function getDbConnection($index)
    {
        return empty($this->dbConnections[$index]) ? null : $this->dbConnections[$index];
    }


    /**
     * Returns peak memory in bytes.
     *
     * @return int
     */
    public function getPeakMemory()
    {
        return $this->hasData('peak_memory') ? $this->getData('peak_memory') : Mage::helper('sheep_debug')->getMemoryUsage();
    }


    /**
     * Returns script execution time in miliseconds
     *
     * @return float
     */
    public function getTime()
    {
        return $this->hasData('time') ? $this->getData('time') : Mage::helper('sheep_debug')->getCurrentScriptDuration();
    }


    /**
     * Returns rendering time in miliseconds
     *
     * @return float
     */
    public function getRenderingTime()
    {
        return $this->hasData('rendering_time') ? $this->getData('rendering_time') : Sheep_Debug_Model_Block::getTotalRenderingTime();
    }


    protected $_eventPrefix = 'sheep_debug_requestInfo';


    protected function _construct()
    {
        $this->_init('sheep_debug/requestInfo');
        $this->design = Mage::getModel('sheep_debug/design');
    }


    /**
     * Generates a unique id that identifies a request.
     *
     * @return string
     */
    public function generateToken()
    {
        return sprintf('%x', crc32(uniqid($this->getController()->getSessionId(), true)));
    }


    /**
     * Serialize fields that are stored in info blob
     *
     * @return string
     */
    public function getSerializedInfo()
    {
        $serializeInfo = serialize(array(
            'request_logging' => $this->getRequestLogging(),
            'action'          => $this->getController(),
            'design'          => $this->getDesign(),
            'blocks'          => $this->getBlocks(),
            'models'          => $this->getModels(),
            'collections'     => $this->getCollections(),
            'connections'     => $this->getDbConnections(),
            'timers'          => $this->getTimers(),
            'stack_log'       => $this->getStackLog(),
            'caches'          => $this->getCaches(),
            'emails'          => $this->getEmails()
        ));

        if (function_exists("gzcompress")) {
            $serializeInfo = gzcompress($serializeInfo);
        }

        return $serializeInfo;
    }

    /**
     * Unserialize info blob
     *
     * @return mixed
     */
    public function getUnserializedInfo()
    {
        $serializeInfo = $this->getInfo();

        if (function_exists('gzuncompress')) {
            $serializeInfo = gzuncompress($serializeInfo);
        }

        return unserialize($serializeInfo);
    }

    /**
     * Returns absolute url for current request
     *
     * @return string
     */
    public function getAbsoluteUrl()
    {
        return Mage::getUrl('', array('_store' => $this->getStoreId(), '_direct' => ltrim($this->getRequestPath(), '/')));
    }


    /**
     * Initialize persistent fields that are used as filters and prepare info blob
     *
     * @return $this
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();

        if (!$this->getId()) {
            $this->setToken($this->generateToken());
            $this->setHttpMethod($this->getController()->getHttpMethod());
            $this->setResponseCode($this->getController()->getResponseCode());
            $this->setIp($this->getController()->getRemoteIp());
        }

        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            $this->setUserAgent($_SERVER['HTTP_USER_AGENT']);
        }

        $this->setRequestPath($this->getController()->getRequestOriginalPath());
        $this->setSessionId($this->getController()->getSessionId());
        $this->setInfo($this->getSerializedInfo());

        return $this;
    }

    /**
     * Initialize fields that are saved in info blob
     *
     * @return Mage_Core_Model_Abstract
     * @throws Mage_Core_Exception
     */
    protected function _afterLoad()
    {
        $info = $this->getUnserializedInfo();

        $this->requestLogging = $info['request_logging'];
        $this->action         = $info['action'];
        $this->design         = $info['design'];
        $this->blocks         = $info['blocks'];
        $this->models         = $info['models'];
        $this->collections    = $info['collections'];
        $this->dbConnections  = $info['connections'];
        $this->timers         = $info['timers'];
        $this->stackLog       = $info['stack_log'];
        $this->caches         = $info['caches'];
        $this->emails         = $info['emails'];

        return parent::_afterLoad();
    }

    /**
     * Save Profiler on first panel rendering when token generated
     * Needed to work correct way with Lesti FPC
     *
     * Full profiler data would be saved on shutdown function
     *
     * @return string
     * @throws Exception
     */
    public function getToken()
    {
        if (!$this->getData('token')) {
            $this->save();
        }

        return $this->getData('token');
    }


    /**
     * @param Exception $e
     */
    public function registerException(Exception $e)
    {
        array_push($this->exceptions, $e);
    }


    /**
     * @return bool
     */
    public function isExceptionOccurred()
    {
        return (bool) count($this->exceptions);
    }
}
