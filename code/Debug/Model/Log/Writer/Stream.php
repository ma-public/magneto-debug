<?php

class Sheep_Debug_Model_Log_Writer_Stream extends Zend_Log_Writer_Stream
{
    protected static $_msgPool = [];
    protected $_logKey;

    /**
     * Sheep_Debug_Model_Log_Writer_Stream constructor.
     * @param $streamOrUrl
     * @param null $mode
     * @throws Zend_Log_Exception
     */
    public function __construct($streamOrUrl, $mode = null)
    {
        parent::__construct($streamOrUrl, $mode);
        $this->_initLogKey($streamOrUrl);
    }

    /**
     * @param array $event  event data
     * @throws Zend_Log_Exception
     */
    protected function _write($event)
    {
        $line = $this->_formatter->format($event);
        self::addMessage($this->_logKey, $line);

        parent::_write($event);
    }

    /**
     * @param array|string|resource $streamOrUrl Stream or URL to open as a stream
     * @return string
     */
    protected function _initLogKey($streamOrUrl)
    {
        if (is_array($streamOrUrl) && isset($streamOrUrl['stream'])) {
            $streamOrUrl = $streamOrUrl['stream'];
        }

        if ($logKey = trim((string) $streamOrUrl)) {
            if ($baseDir = rtrim(Mage::getBaseDir(), "/\\")) {
                $baseDir .= '/';
                if ((strpos($logKey, $baseDir) === 0) && (strlen($logKey) > strlen($baseDir))) {
                    $logKey = str_replace($baseDir, '', $logKey);
                }
            }
        } else {
            $logKey = 'unidentified/misc';
        }

        $this->_logKey = $logKey;
    }

    /**
     * @param string $message
     * @param string $key
     */
    protected static function addMessage($key, $message)
    {
        self::$_msgPool[$key][] = $message;
    }

    /**
     * @return array
     */
    public static function getMessages()
    {
        return self::$_msgPool;
    }
}
