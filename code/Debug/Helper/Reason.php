<?php

class Sheep_Debug_Helper_Reason extends Mage_Core_Helper_Data
{
    const ERROR_OCCURRED = 1;
    const EXCEPTION_OCCURRED = 2;
    const TIME_EXCEEDED = 4;
    const TOOLBAR_ENABLED = 8;


    protected $_dict = [
        self::ERROR_OCCURRED => 'Errors',
        self::EXCEPTION_OCCURRED => 'Exception',
        self::TIME_EXCEEDED => 'Timelimit',
        self::TOOLBAR_ENABLED => 'Toolbar'
    ];

    /**
     * @param $inCode
     * @return string
     */
    public function dict($inCode)
    {
        if (!($inCode && is_numeric($inCode))) {
            return '';
        }

        $res = [];
        foreach ($this->_dict as $code => $value) {
            if (($code & $inCode) === $code) {
                array_push($res, $value);
            }
        }

        return implode(', ', $res);
    }
}
