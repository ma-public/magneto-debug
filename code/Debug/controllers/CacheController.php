<?php

class Sheep_Debug_CacheController extends Sheep_Debug_Controller_Front_Action
{

    public function cleanAction()
    {
        try {
            $type  = $this->getRequest()->getParam('type', '');
            Mage::app()->getCacheInstance()->cleanType($type);

            $this->getSession()->addSuccess("Cache {$type} cleaned.");
        } catch (Exception $e) {
            $message = $this->__('Cache cannot be cleaned: %s', $e->getMessage());
            $this->getSession()->addError($message);
        }

        $this->_redirectReferer();
    }

    public function flushAction()
    {
        try {
            Mage::app()->getCacheInstance()->flush();

            $this->getSession()->addSuccess('Cache flushed.');
        } catch (Exception $e) {
            $message = $this->__('Cache cannot be flushed: %s', $e->getMessage());
            $this->getSession()->addError($message);
        }

        $this->_redirectReferer();
    }

    public function enableAction()
    {
        try {
            $type  = $this->getRequest()->getParam('type', '');
            $this->setCacheStatus(array($type), 1);

            $this->getSession()->addSuccess("Cache {$type} enabled.");
        } catch (Exception $e) {
            $message = $this->__('Cache cannot be enabled: %s', $e->getMessage());
            $this->getSession()->addError($message);
        }

        $this->_redirectReferer();
    }

    public function disableAction()
    {
        try {
            $type  = $this->getRequest()->getParam('type', '');
            $this->setCacheStatus(array($type), 0);

            $this->getSession()->addSuccess("Cache {$type} disabled.");
        } catch (Exception $e) {
            $message = $this->__('Cache cannot be disabled: %s', $e->getMessage());
            $this->getSession()->addError($message);
        }

        $this->_redirectReferer();
    }

    protected function setCacheStatus(array $types, $status)
    {
        $allTypes = Mage::app()->getCacheInstance()->getTypes();
        $allTypes = array_map(function (Varien_Object $type) {
            return $type->getData('status');
        }, $allTypes);

        $updatedTypes = 0;
        foreach ($types as $code) {
            if (isset($allTypes[$code])) {
                $allTypes[$code] = $status;
                $updatedTypes++;
            }
        }
        if ($updatedTypes > 0) {
            Mage::app()->saveUseCache($allTypes);
        }
    }

}
