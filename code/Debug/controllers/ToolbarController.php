<?php

class Sheep_Debug_ToolbarController extends Sheep_Debug_Controller_Front_Action
{

    /**
     * Request toolbar
     */
    public function indexAction()
    {
        $token = (string)$this->getRequest()->getParam('token');
        if (!$token) {
            $this->getResponse()->setHttpResponseCode(400);
            return $this->_getRefererUrl();
        }

        /** @var Sheep_Debug_Model_RequestInfo $requestInfo */
        $requestInfo = Mage::getModel('sheep_debug/requestInfo')->load($token, 'token');
        if (!$requestInfo->getId()) {
            $this->getResponse()->setHttpResponseCode(404);
            return $this->_getRefererUrl();
        }

        Mage::register('sheep_debug_request_info', $requestInfo);

        $this->loadLayout();

        return $this->getResponse()
             ->setBody($this->getLayout()->getBlock('debug_panels')->toHtml());
    }

}
